
public class Alumno {
			
	String nombre;
	String apellidoPaterno;
	String apellidoMaterno;
	String nombreCurso;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombreCurso() {
		return nombreCurso;
	}
	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}
	
	@Override
	public String toString() {
		StringBuffer mensaje = new StringBuffer();		
		mensaje.append("{"+this.nombre+"}");
		mensaje.append("{"+this.apellidoPaterno+"}");
		mensaje.append("{"+this.apellidoMaterno+"}");
		mensaje.append("{"+this.nombreCurso+"}");
		return mensaje.toString();
	}
	
}
