import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PrincipalGame {
	
	
	interface Playeable {
		
		void walk(double x, double y);
		
		default void setGameName(String name) {
			System.out.println(name);
		}
	}
	
	 interface Gameable {
			void startGame();
			
			default void setGameName(String name) {			
				System.out.println(name);
			}
		}
	 
	 interface Soundable {
			void playMusic(String song);
			
			default void setGameName(String name) {
				System.out.println(name);
			}
		}
	
	static String gameName = "Hola";
	static String song = null;
	static double playerPosx = 0;
	static double playerPosY = 0;
	
	
	static final String[] nombres = { "Julio",
			"Aldo",
			"Jesus",
			"Gerardo",
			"Adriana",
			"Edgar",
			"Antonio",
			"Hugo",
			"Omar",
			"Diana",
			"Cesar",
			"Thelma",
			"Oscar",
			"Carlos",
			"Sergio",
			"Maria",
			"Luis",
			"Alfonso",
			"Francisca",
			"Margarita"
	};
			
	public static void main(String [] args) {
		
		//Inicio Ejercicio 1
		System.out.println(">>>>> Ejercicio 1 <<<<<");
		Playeable p = (x,y) -> {
			playerPosx = playerPosx + x;
			playerPosY = playerPosY + y;
		};		
		
		p.walk(1, 2);
		System.out.println(playerPosx + " " + playerPosY);
		
		
		Gameable g = () -> { p.setGameName(gameName);
			};
		
		g.startGame();
		
		Soundable s = (song) ->{
			Optional <String> aux = Optional.ofNullable(song);
			
			if(!aux.isPresent()) {
				song = "new song";
			}			
			System.out.println(song);
		};
		
		s.playMusic(song);
		
		
		//Fin Ejercicio 1
		
		
		// Inicio Ejercicio 2
		System.out.println(">>>>> Ejercicio 2 <<<<<");
		List<Alumno> listAlumn = new ArrayList();		
		Alumno alumno;
		
		for(int i = 0 ; i<20 ; i++) {
			alumno = new Alumno();
			alumno.setNombre(nombres[i]);
			alumno.setApellidoPaterno("Figueroa");
			alumno.setApellidoMaterno("Del Castillo");
			alumno.setNombreCurso("Curso Java");
			listAlumn.add(alumno);
		}
		
		/*
		 *  2.1 Imprime el contenido de todos los alumnos de la lista.
		 */
		System.out.println("2.1 >>>>>>>>>>>>");
		listAlumn.stream().forEach(alum -> System.out.println(alum.toString()));
	

		/*
		 *  2.2 Imprime la longitud de la lista de alumnos.
		 */
		System.out.println("2.2 >>>>>>>>>>>>");
		System.out.println("Longitud Arreglo " + listAlumn.size());
		

		/*
		 *  2.3 Imprime los alumnos cuyo nombre termine con la letrá "a".
		 */
		System.out.println("2.3 >>>>>>>>>>>>");
		listAlumn.stream().filter(alum -> alum.getNombre().endsWith("a")).forEach(System.out::println);
		
		
		/*
		 *  2.3 Imprime los primeros 5 alumnos de la lista.
		 */
		System.out.println("2.4 >>>>>>>>>>>>");
		listAlumn.stream().limit(5).forEach(System.out::println);
		
	}

}
